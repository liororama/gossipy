import ctypes


def get_null_ending_list(l):
    """
    Take a list and convert it to an array with None at the end
    """
    arr = (ctypes.c_char_p * (len(l) + 1))()
    arr[:-1] = l
    arr[len(l)] = None
    return arr


def get_xml_info(server="localhost", port=12000, item_list=None):
    """
    Connect to gossimon infod daemon and return xml cluster info

    This function provides access to the C implementation of infoxml_all,
    which connects to infod on the given server and port and retreive an
    xml string containing information about the cluster nodes
    """

    lib = ctypes.CDLL('libgossimon_client.so')
    lib.infoxml_all.restype = ctypes.c_char_p
    if item_list is not None:
        item_arr = get_null_ending_list(item_list)
    else:
        item_arr = None

    s = lib.infoxml_all(server, port, item_arr)
    return s

